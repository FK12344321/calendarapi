<h1>Calendar API</h1>
<h2>Project description</h2>
<p>The given project provides an API for calendar management. It allows the users to:</p>
<ul>
    <li>create and delete users</li>
    <li>update users' info</li>
    <li>get users' info</li>
    <li>create and delete meetings</li>
    <li>update meetings' info</li>
    <li>get meetings' info</li>
    <li>send meeting invitation</li>
    <li>accept and decline meeting invitation</li>
    <li>find all the meetings in specific time interval for a user</li>
    <li>find appropriate meeting time for a set of users based on meetings they have accepted</li>
</ul>
<h2>Project installation</h2>
<p>To start using the project you need to start database (db) 
docker service located in docker-compose.yml file.<br> Basically, you just need 
to run the following command in root directory of your repository:</p>
<code>docker-compose up --build</code>
<p>Now you can start the server itself</p>
<p>To launch the http server itself you need to run the project with sbt. If you do not <br>
have it, you can download it. To run the server you need to run the following line in<br>
the root directory of the project.</p>
<code>sbt run</code> 
<p>Now you can send http requests to the server!!!</p>
<h2>Project usage</h2>
<p>To use the api you need to send http requests to the server to change its state<br>
and get json responses.</p>
<h3>Examples of the all API methods provided by the service:</h3>
<ul>
<li>
    Create user with provided json<br>
    POST "http://127.0.0.1:8080/create-user"<br>
    Request content: '{"emailAddress":"email_address_example","name":"name_example","userId":1}'<br>
    Response content: "user created" 
</li>
<li>
    Delete user by id provided in the URL<br>
    POST "http://127.0.0.1:8080/delete-user/1"<br>
    Note: if you delete user, all the meetings that he has created are deleted as well. <br>
    In case of user deletion. Meetings' statuses he was invited to get reevaluated. <br>
    Response content: "user deleted"
</li>
<p>You can find all the API method description <a href="openapi.yaml">here</a>
<p>P.S. you should notice that all the dates are stored in milliseconds. This is
the most flexible way to store dates since almost any programming 
language provides you with tools letting you format the 
date as you wish.</p>
<h2>Email</h2>
<p>Also remember that as soon as you send an invitation, the invitee will get an email notification 
about the event he/she is invited to.</p>
