package calendar_api.domain

case class Meeting (
                    meetingId: Long, name: String,
                    ownerId: Long,
                    startDate: Long, endDate: Long,
                    place: Option[String], description: Option[String],
                    frequency: Option[Long], status: Option[String]
                  )

