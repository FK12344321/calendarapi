package calendar_api.domain

case class Invitation(
                       invitationId: Long, invitorId: Long,
                       inviteeId: Long, meetingId: Long, accepted: Option[Boolean],
                       receivedStatus: Option[String]
                     )
