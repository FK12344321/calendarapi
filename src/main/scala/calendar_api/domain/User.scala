package calendar_api.domain

case class User(userId: Long, name: String, emailAddress: String)
