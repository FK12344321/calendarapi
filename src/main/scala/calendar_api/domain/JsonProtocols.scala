package calendar_api.domain

import spray.json.DefaultJsonProtocol

trait JsonProtocols extends DefaultJsonProtocol {
  implicit val userFormat = jsonFormat3(User)
  implicit val meetingFormat = jsonFormat9(Meeting)
  implicit val invitationFormat = jsonFormat6(Invitation)
}
