package calendar_api

import calendar_api.domain.{Meeting, User}
import calendar_api.repository.{DatabaseInvitationRepository, DatabaseMeetingRepository, DatabaseUserRepository}
import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.http.javadsl.marshalling.Marshaller
import akka.http.scaladsl.Http
import io.getquill.context.jdbc.JdbcContext
import io.getquill.{MirrorContext, MysqlJdbcContext, SnakeCase}

import scala.io.StdIn

object Main extends App {
  val configPrefix = "ctx"
  val userRepository = new DatabaseUserRepository(configPrefix)
  val meetingRepository = new DatabaseMeetingRepository(configPrefix)
  val invitationRepository = new DatabaseInvitationRepository(configPrefix)

  implicit val actorSystem = ActorSystem(Behaviors.empty, "akka-http")
  implicit val stringMarshaller = Marshaller
  implicit val executionContext = actorSystem.executionContext

  val server = new calendar_api.http.HttpServer (
    userRepository, meetingRepository, invitationRepository
  )
  val bindingFuture = Http().newServerAt("localhost", 8080).bind(server.routes)

  StdIn.readLine()

  bindingFuture
    .flatMap(_.unbind()) // trigger unbinding from the port
    .onComplete(_ => actorSystem.terminate()) // and shutdown when done


}