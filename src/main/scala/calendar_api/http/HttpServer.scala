package calendar_api.http

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.http.javadsl.marshalling.Marshaller
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives.{post, _}
import spray.json._
import calendar_api.domain._
import calendar_api.repository._
import akka.Done
import akka.http.scaladsl.server.Route
import calendar_api.email.InvitationSender

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.util.Try

class HttpServer(
                userRepository: UserRepository,
                meetingRepository: MeetingRepository,
                invitationRepository: InvitationRepository
                ) extends calendar_api.domain.JsonProtocols with SprayJsonSupport {

  implicit val actorSystem: ActorSystem[Nothing] = ActorSystem(Behaviors.empty, "akka-http")
  implicit val stringMarshaller: Marshaller.type = Marshaller
  implicit val executionContext: ExecutionContextExecutor = actorSystem.executionContext

  val routes: Route = {
    concat (
      get {
        path("user" / LongNumber) {
          id =>
            val maybeUser: Future[Option[User]] = Future {
              userRepository.getUser(id)
            }

            onSuccess(maybeUser) {
              case Some(user) => complete(user)
              case None       => complete(StatusCodes.NotFound)
            }
        }
      },

      post {
        path("user") {
          entity(as[User]) { user =>
            val createUser: Future[Done] = Future {
              userRepository.createUser(user)
              Done
            }

            onSuccess(createUser) { _ =>
              complete("user created")
            }
          }
        }
      },

      put {
        path("user") {
          entity(as[User]) { user =>
            val updateUser: Future[Done] = Future {
              userRepository.updateUser(user.userId, user)
              Done
            }

            onSuccess(updateUser) { _ =>
              complete("user updated")
            }
          }
        }
      },

      delete {
        path("user" / LongNumber) {
          id =>
            val deleteUser: Future[Done] = Future {
              userRepository.deleteUser(id)
              Done
            }

            onSuccess(deleteUser) { _ =>
              complete("user deleted")
            }
        }
      },

      post {
        path("meeting") {
          entity(as[Meeting]) { meeting =>
            val createMeeting: Future[Done] = Future {
              meetingRepository.createMeeting(meeting)

              Done
            }

            onSuccess(createMeeting) { _ =>
              complete("meeting created")
            }
          }
        }
      },

      delete {
        path("meeting" / LongNumber) {
          id =>
            val deleteMeeting: Future[Done] = Future {
              meetingRepository.deleteMeeting(id)
              Done
            }

            onSuccess(deleteMeeting) { _ =>
              complete("meeting deleted")
            }
        }
      },

      get {
        path("meeting" / LongNumber) {
          id =>
            val maybeMeeting: Future[Option[Meeting]] = Future {
              meetingRepository.getMeeting(id)
            }

            onSuccess(maybeMeeting) {
              case Some(meeting) => complete(meeting)
              case None       => complete(StatusCodes.NotFound)
            }
        }
      },

      put {
        path("meeting") {
          entity(as[Meeting]) { meeting =>
            val updateMeeting: Future[Done] = Future {
              meetingRepository.updateMeeting(meeting.meetingId, meeting)
              Done
            }

            onSuccess(updateMeeting) { _ =>
              complete("meeting updated")
            }
          }
        }
      },

      put {
        pathPrefix("accept-invitation") {
          parameters("invitation-id".as[Long], "received-status") {
            (id: Long, receivedStatus: String) =>
              val acceptInvitation = Future {
                invitationRepository.acceptInvitation(id, receivedStatus)
                Done
              }

              onSuccess(acceptInvitation) { _ =>
                complete("invitation accepted")
              }
          }
        }
      },

      put {
        path("decline-invitation" / LongNumber) { id =>
          val declineInvitation = Future {
            invitationRepository.declineInvitation(id)
            Done
          }

          onSuccess(declineInvitation) { _ =>
            complete("invitation declined")
          }
        }
      },

      get {
        pathPrefix("time-interval-meetings") {
          parameters("user-id".as[Long], "start-time".as[Long], "end-time".as[Long]) {
            (id: Long, start: Long, end: Long) =>
              val maybeMeetings = Future {
                meetingRepository.findMeetingsByTimeInterval(id, start, end)
              }

              onSuccess(maybeMeetings) { meetings: List[Long] =>
                meetings match {
                  case Nil => complete("no meetings")
                  case meetings =>
                    complete(meetings.toJson)
                }
              }
          }
        }
      },

      get {
        pathPrefix("closest-suitable-meeting") {
          parameters("duration".as[Long], "user-id".as[Long].repeated) {
            (duration, usersId) =>
              val maybeDates: Future[Option[(Long, Long)]] = Future {
                userRepository.findClosestFreeTimeInterval(usersId.toList, duration)
              }

              onSuccess(maybeDates) {
                case Some((start, end)) =>
                  complete(JsObject (
                    "start" -> start.toJson,
                    "end" -> end.toJson
                  ))
                case None =>
                  complete(StatusCodes.BadRequest)
              }
          }
        }
      },

      post {
        path("invitation") {
          entity(as[Invitation]) { invitation =>
            val createInvitation: Future[Done] = Future {
              invitationRepository.createInvitation(invitation)

              Try {
                val meeting = meetingRepository.getMeeting(invitation.meetingId)
                val invitor = userRepository.getUser(invitation.invitorId)
                val invitee = userRepository.getUser(invitation.inviteeId)
                val sender = new InvitationSender(invitation, meeting.get, invitor.get, invitee.get)
                sender.sendMessage()
              }

              Done
            }

            onSuccess(createInvitation) { _ =>
              complete("invitation created")
            }
          }
        }
      },

      get {
        path("invitation" / LongNumber) { id =>
          val maybeInvitation = Future {
            invitationRepository.getInvitation(id)
          }

          onSuccess(maybeInvitation) { invitation: Option[Invitation] =>
            invitation match {
              case None => complete(StatusCodes.NotFound)
              case Some(invitation) => complete(invitation)
            }
          }
        }
      },

      delete {
        path("invitation" / LongNumber) { id =>
          val deleteInvitation = Future {
            invitationRepository.deleteInvitation(id)
            Done
          }

          onSuccess(deleteInvitation) { _ =>
            complete("invitation deleted")
          }
        }
      }
    )
  }
}
