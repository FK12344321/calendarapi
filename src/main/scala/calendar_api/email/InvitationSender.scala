package calendar_api.email
// acv3367A&

import calendar_api.domain.{Invitation, Meeting, User}

import javax.mail._
import javax.mail.internet._
import java.util.Date
import java.util.Properties

class InvitationSender(invitation: Invitation, meeting: Meeting, invitor: User, invitee: User)
{
  var message: Message = null
  val from = "calendar.api.hw@gmail.com"
  val smtpHost = "smtp.gmail.com"
  val port = "587"

  message = createMessage
  message.setFrom(new InternetAddress(from))
  setToCcBccRecipients()

  message.setSentDate(new Date())
  message.setSubject("Invitation to the meeting")
  message.setContent(formatInvitation(invitation, meeting, invitor, invitee), "text/html")

  def sendMessage(): Unit = {
    Transport.send(message)
  }

  def createMessage: Message = {
    val props = new Properties
    props.put("mail.smtp.auth", "true")
    props.put("mail.smtp.starttls.enable", "true")
    props.put("mail.smtp.host", smtpHost)
    props.put("mail.smtp.port", port)
    props.put("mail.smtp.ssl.trust", "smtp.gmail.com")
    props.put("mail.smtp.ssl.protocols", "TLSv1.2")
    val session = Session.getInstance(props,
      new javax.mail.Authenticator() {
        override def getPasswordAuthentication: javax.mail.PasswordAuthentication = {
          new PasswordAuthentication("calendar.api.hw@gmail.com", "acv3367A&")
        }
      })
    new MimeMessage(session)
  }

  def setToCcBccRecipients(): Unit = {
    setMessageRecipients(invitee.emailAddress, Message.RecipientType.TO)
  }

  def setMessageRecipients(recipient: String, recipientType: Message.RecipientType): Unit = {
    val addressArray = buildInternetAddressArray(recipient).asInstanceOf[Array[Address]]
    if ((addressArray != null) && (addressArray.length > 0))
    {
      message.setRecipients(recipientType, addressArray)
    }
  }

  def buildInternetAddressArray(address: String): Array[InternetAddress] = {
    InternetAddress.parse(address)
  }

  def formatInvitation(invitation: Invitation, meeting: Meeting, invitor: User, invitee: User): String = {
    s"Dear ${invitee.name},<br>" +
      s"<p>${invitor.name} has invited you to the ${meeting.name}<br>" +
      s"If you wan to respond, the your invitation id is ${invitation.invitationId}</p>"
  }
}