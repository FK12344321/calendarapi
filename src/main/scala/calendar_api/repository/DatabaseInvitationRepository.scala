package calendar_api.repository

import calendar_api.domain.{Invitation, Meeting}
import io.getquill.{MysqlJdbcContext, SnakeCase}

class DatabaseInvitationRepository(ctxConfName: String)
  extends InvitationRepository {

  lazy val ctx = new MysqlJdbcContext(SnakeCase, ctxConfName)
  import ctx._

  override def acceptInvitation(invitationId: Long, receivedStatus: String): Unit = {
    run (
      quote {
        query[Invitation].filter(_.invitationId == lift(invitationId))
          .update(_.accepted -> Some(true), _.receivedStatus -> Some(lift(receivedStatus)))
      }
    )

    CommonOps.reevaluateMeetingStatus(getInvitation(invitationId).get.meetingId)(ctx)
  }

  override def declineInvitation(invitationId: Long): Unit = {
    run (
      quote {
        query[Invitation].filter(_.invitationId == lift(invitationId))
          .update(_.accepted -> Some(false))
      }
    )

    CommonOps.reevaluateMeetingStatus(getInvitation(invitationId).get.meetingId)(ctx)
  }

  override def createInvitation(invitation: Invitation): Unit = {
    run (
      quote {
        query[Invitation].insertValue(lift(invitation))
      }
    )

    CommonOps.reevaluateMeetingStatus(invitation.meetingId)(ctx)
  }

  override def getInvitation(invitationId: Long): Option[Invitation] = {
    run (
      quote {
        query[Invitation].filter(_.invitationId == lift(invitationId))
      }
    ).headOption
  }

  override def deleteInvitation(invitationId: Long): Unit = {
    val meetingId = getInvitation(invitationId).get.meetingId

    run (
      quote {
        query[Invitation].filter(_.invitationId == lift(invitationId)).delete
      }
    )

    CommonOps.reevaluateMeetingStatus(meetingId)(ctx)
  }

  def deleteAll(): Unit = {
    run(
      quote {
        query[Invitation].delete
      }
    )
  }
}
