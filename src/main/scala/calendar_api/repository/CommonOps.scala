package calendar_api.repository

import calendar_api.domain.{Invitation, Meeting}
import io.getquill.{MysqlJdbcContext, SnakeCase}

object CommonOps {
  def meetingConfirmed(meetingId: Long)(implicit ctx: MysqlJdbcContext[SnakeCase.type]): Boolean = {
    import ctx._

    val invitations = run (
      quote {
        query[Invitation].filter(_.meetingId == lift(meetingId))
      }
    )

    invitations.forall(_.accepted.getOrElse(false))
  }

  def reevaluateMeetingStatus(meetingId: Long)(implicit ctx: MysqlJdbcContext[SnakeCase.type]): Unit = {
    import ctx._

    run (
      quote {
        query[Meeting]
          .filter(_.meetingId == lift(meetingId))
          .update(_.status -> Some(if (lift(meetingConfirmed(meetingId))) "Confirmed" else "In doubt"))
      }
    )
  }
}
