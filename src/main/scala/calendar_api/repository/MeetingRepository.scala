package calendar_api.repository

import calendar_api.domain.Meeting

trait MeetingRepository {
  def createMeeting(meeting: Meeting): Unit
  def deleteMeeting(meetingId: Long): Unit
  def updateMeeting(meetingId: Long, updatedMeeting: Meeting): Unit
  def getMeeting(meetingId: Long): Option[Meeting]
  def findMeetingsByTimeInterval(userId: Long, startTime: Long, endDate: Long): List[Long]
}
