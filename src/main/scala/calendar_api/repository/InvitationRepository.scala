package calendar_api.repository

import calendar_api.domain.Invitation

trait InvitationRepository{
  def acceptInvitation(invitationId: Long, receivedStatus: String): Unit
  def declineInvitation(invitationId: Long): Unit
  def createInvitation(invitation: Invitation): Unit
  def getInvitation(invitationId: Long): Option[Invitation]
  def deleteInvitation(invitationId: Long): Unit
}
