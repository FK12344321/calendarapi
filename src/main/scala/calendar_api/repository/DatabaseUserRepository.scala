package calendar_api.repository

import calendar_api.domain.{Invitation, Meeting, User}
import io.getquill.{MysqlJdbcContext, SnakeCase}

import scala.annotation.tailrec
import calendar_api.time._

import scala.util.{Failure, Success, Try}

class DatabaseUserRepository(ctxConfName: String, timeProvider: TimeProvider = new RealTimeProvider)
  extends UserRepository {

  lazy val ctx = new MysqlJdbcContext(SnakeCase, ctxConfName)
  import ctx._

  override def createUser(user: User): Unit = {
    run (
      quote {
        query[User].insertValue(lift(user))
      }
    )
  }

  def getAffectedMeetings(userId: Long): List[Long] = {
    run (
      quote {
        query[Invitation]
          .filter(_.inviteeId == lift(userId))
          .distinct
          .map(_.meetingId)
      }
    )
  }

  override def deleteUser(userId: Long): Unit = {
    val affectedMeetings = getAffectedMeetings(userId)

    run (
      quote {
        query[User].filter(_.userId == lift(userId)).delete
      }
    )

    affectedMeetings.foreach(meetingId => {
      CommonOps.reevaluateMeetingStatus(meetingId)(ctx)
    })
  }

  override def updateUser(userId: Long, updatedUser: User): Unit = {
    run (
      quote {
        query[User].filter(_.userId == lift(userId)).updateValue(lift(updatedUser))
      }
    )
  }

  override def getUser(userId: Long): Option[User] = {
    run (
      quote {
        query[User].filter(_.userId == lift(userId))
      }
    ).headOption
  }

  @tailrec
  private def findAppropriateInterval(meetingTimeIntervals: LazyList[(Long, Long)], requiredDuration: Long): (Long, Long) = {
    meetingTimeIntervals match {
      case meeting1 #:: meeting2 #:: _ =>
        val difference = meeting2._1 - meeting1._2
        if (difference >= requiredDuration)
          (meeting1._2, meeting1._2 + requiredDuration)
        else
          findAppropriateInterval(meetingTimeIntervals.tail, requiredDuration)
      case meeting +: LazyList() => (meeting._2, meeting._2 + requiredDuration)
    }
  }

  def getUsersInvolvedMeetings(usersId: List[Long]): List[Meeting] = {
    run (
      quote {
        query[Invitation]
          .filter(
            invitation =>
              invitation.accepted.getOrElse(false) && liftQuery(usersId).contains(invitation.inviteeId) &&
                invitation.receivedStatus.getOrElse("No response") != "Free" ||
                liftQuery(usersId).contains(invitation.invitorId)
          )
          .join(query[Meeting])
          .on((invitation, meeting) => invitation.meetingId == meeting.meetingId)
          .map(el => el._2)
          .distinct
          .sortBy(_.startDate)
      }
    )
  }

  def getNextRepeatedMeetingTime(repeatingMeetings: List[(Long, Long, Option[Long])],
                                 currentTime: Long, meetingDuration: Long): Option[(Long, Long)] = {

    repeatingMeetings.filter(_._1 < currentTime).map(meeting => {
      if (meeting._3.get < meetingDuration) throw new IllegalArgumentException
      val start = meeting._1 + meeting._3.get * ((currentTime - meeting._1) / meeting._3.get + (if ((currentTime - meeting._1) % meeting._3.get != 0) 1 else 0))
      val end = start + meeting._2 - meeting._1
      (start, end)
    }
    ).sortBy(_._1).headOption
  }

  def constructNextElements(nextRepeatedMeetingTime: Option[(Long, Long)],
                           nextNonRepeatedMeeting: Option[(Long, Long, Option[Long])],
                           futureMeetings: List[(Long, Long, Option[Long])] ,
                           repeatingMeetings: List[(Long, Long, Option[Long])],
                           meetingDuration: Long
                          ): LazyList[(Long, Long)] = {

    (nextRepeatedMeetingTime, nextNonRepeatedMeeting) match {
      case (None, None) => LazyList()
      case (None, Some(meeting)) =>
        (meeting._1, meeting._2) #:: constructSchedule(futureMeetings.tail, meeting._2, repeatingMeetings, meetingDuration)
      case (Some(meeting), None) =>
        (meeting._1, meeting._2) #:: constructSchedule(Nil, meeting._2, repeatingMeetings, meetingDuration)
      case (Some(_), Some(meeting)) =>
        (meeting._1, meeting._2) #:: constructSchedule(futureMeetings.tail, meeting._2, repeatingMeetings, meetingDuration)

    }
  }

  def constructSchedule(
                         currentMeetings: List[(Long, Long, Option[Long])],
                         currentTime: Long, repeatingMeetings: List[(Long, Long, Option[Long])] ,
                         meetingDuration: Long
                       ): LazyList[(Long, Long)] = {
    val futureMeetings = currentMeetings.filter(meeting =>
      meeting._2 > currentTime
    )
    val nextNonRepeatedMeeting = futureMeetings.headOption
    val nextRepeatedMeetingTime = getNextRepeatedMeetingTime(repeatingMeetings, currentTime, meetingDuration)

    constructNextElements(nextRepeatedMeetingTime, nextNonRepeatedMeeting, futureMeetings, repeatingMeetings, meetingDuration)
  }

  override def findClosestFreeTimeInterval(usersId: List[Long], meetingDuration: Long): Option[(Long, Long)] = {
    val meetings = getUsersInvolvedMeetings(usersId)
      .map(el => (el.startDate, el.endDate, el.frequency))

    val currentTime = timeProvider.getCurrentMillsTime
    val repeatingMeetings = meetings.filter(_._3.isDefined)

    lazy val meetingsTime: LazyList[(Long, Long)] = constructSchedule(meetings, currentTime, repeatingMeetings, meetingDuration)

    Try[(Long, Long)] {
      findAppropriateInterval(meetingsTime, meetingDuration)
    } match {
      case Success((start, end)) => Some(start, end)
      case Failure(_) => None
    }
  }

  def deleteAll(): Unit = {
    run(
      quote {
        query[User].delete
      }
    )
  }
}
