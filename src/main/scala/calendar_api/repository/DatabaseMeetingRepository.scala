package calendar_api.repository
import calendar_api.domain.{Invitation, Meeting}
import io.getquill.{MysqlJdbcContext, SnakeCase}

class DatabaseMeetingRepository(ctxConfName: String)
  extends MeetingRepository {

  lazy val ctx = new MysqlJdbcContext(SnakeCase, ctxConfName)
  import ctx._

  override def createMeeting(meeting: Meeting): Unit = {
    val updatedMeeting = meeting.copy(status = Some("Confirmed"))

    run (
      quote {
        query[Meeting].insertValue(lift(updatedMeeting))
      }
    )
  }

  override def deleteMeeting(meetingId: Long): Unit = {
    run (
      quote {
        query[Meeting].filter(_.meetingId == lift(meetingId)).delete
      }
    )

    run (
      quote {
        query[Invitation].filter(_.meetingId == lift(meetingId)).delete
      }
    )
  }

  def needsUpdate(initialMeeting: Meeting, updatedMeeting: Meeting, meetingId: Long): Boolean = {
    val essentialInfoChanged =
      updatedMeeting.startDate != initialMeeting.startDate ||
      updatedMeeting.endDate != initialMeeting.endDate ||
      updatedMeeting.place != initialMeeting.place

    val hasInvitees = run (
      quote {
        query[Invitation].filter(_.meetingId == lift(meetingId))
      }
    ).nonEmpty

    essentialInfoChanged && hasInvitees
  }

  def cancelInvitationsAcceptance(meetingId: Long): Unit = {
    run (
      quote {
        query[Invitation]
          .filter(_.meetingId == lift(meetingId))
          .update(_.accepted -> None, _.receivedStatus -> None)
      }
    )
  }

  def updateMeetingStatus(meetingId: Long, updatedMeetingStatus: String) = {
    run (
      quote {
        query[Meeting]
          .filter(_.meetingId == lift(meetingId))
          .update(_.status -> Some(lift(updatedMeetingStatus)))
      }
    )
  }

  def actualMeetingUpdate(meetingId: Long, updatedMeeting: Meeting): Unit = {
    run (
      quote {
        query[Meeting]
          .filter(_.meetingId == lift(meetingId))
          .updateValue(lift(updatedMeeting))
      }
    )
  }

  override def updateMeeting(meetingId: Long, updatedMeeting: Meeting): Unit = {
    val initialMeeting = getMeeting(meetingId).get
    actualMeetingUpdate(meetingId, updatedMeeting)
    val meetingNeedsUpdate: Boolean = needsUpdate(initialMeeting, updatedMeeting, meetingId)
    val updatedMeetingStatus = if (meetingNeedsUpdate) "In doubt" else "Confirmed"
    if (meetingNeedsUpdate) cancelInvitationsAcceptance(meetingId)
    updateMeetingStatus(meetingId, updatedMeetingStatus)
  }

  override def getMeeting(meetingId: Long): Option[Meeting] = {
    run (
      quote {
        query[Meeting].filter(_.meetingId == lift(meetingId))
      }
    ).headOption
  }

  override def findMeetingsByTimeInterval(userId: Long, startTime: Long, endTime: Long): List[Long] = {
    run (
      quote {
        query[Invitation]
          .filter(
            invitation =>
              invitation.accepted.getOrElse(false) && invitation.inviteeId == lift(userId) ||
                invitation.invitorId == lift(userId)
          )
          .distinct
          .join(query[Meeting])
          .on((invitation, meeting) => invitation.meetingId == meeting.meetingId)
          .map(_._2)
          .filter(
            meeting => {
              val meetingStart = meeting.startDate
              val meetingEnd = meeting.endDate
              val initialTime = lift(startTime)
              val finishTime = lift(endTime)
              (meetingStart >= initialTime && meetingStart <= finishTime) ||
                (meetingStart <= initialTime && meetingEnd <= finishTime) ||
                (meetingEnd <= initialTime && meetingEnd <= finishTime) ||
                (meetingStart >= initialTime && meetingEnd <= finishTime)
            }
          )
          .map(_.meetingId)
      }
    )
  }

  def deleteAll(): Unit = {
    run(
      quote {
        query[Meeting].delete
      }
    )
  }
}
