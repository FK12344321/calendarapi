package calendar_api.repository

import calendar_api.domain.User

trait UserRepository {
  def createUser(user: User): Unit
  def deleteUser(userId: Long): Unit
  def updateUser(userId: Long, updatedUser: User): Unit
  def getUser(userId: Long): Option[User]
  def findClosestFreeTimeInterval(usersId: List[Long], meetingDuration: Long): Option[(Long, Long)]
}
