package calendar_api.time

class RealTimeProvider extends TimeProvider {
  override def getCurrentMillsTime: Long = System.currentTimeMillis()
}
