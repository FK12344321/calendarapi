package calendar_api.time

trait TimeProvider {
  def getCurrentMillsTime: Long
}
