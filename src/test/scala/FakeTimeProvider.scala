import calendar_api.time.TimeProvider

class FakeTimeProvider extends TimeProvider {
  override def getCurrentMillsTime: Long = 1651352123456L
}