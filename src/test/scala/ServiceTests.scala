import calendar_api.repository.{DatabaseInvitationRepository, DatabaseMeetingRepository, DatabaseUserRepository}
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, StatusCodes}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import spray.json._

class ServiceTests extends AnyWordSpec with Matchers with ScalaFutures with ScalatestRouteTest {
  val configPrefix = "ctx_test"
  val fakeTimeProvider = new FakeTimeProvider
  val userRepository = new DatabaseUserRepository(configPrefix, timeProvider = fakeTimeProvider)
  val meetingRepository = new DatabaseMeetingRepository(configPrefix)
  val invitationRepository = new DatabaseInvitationRepository(configPrefix)

  val server = new calendar_api.http.HttpServer(
    userRepository, meetingRepository, invitationRepository
  )

  def prepareDatabase(): Unit = {
    invitationRepository.deleteAll()
    meetingRepository.deleteAll()
    userRepository.deleteAll()
  }

  def addJsonAttribute(json: String, key: String, value: String): String = {
    json.dropRight(1) + s""","$key":"$value"}"""
  }

  "The service" should {

    prepareDatabase()

    val users: List[String] = List(
      """{"emailAddress":"tred_b12@gmail.com","name":"Tred Burn","userId":1}""",
      """{"emailAddress":"trotskiy222@yandex.ru","name":"Leo Trotskiy","userId":2}""",
      """{"emailAddress":"abdula_mor@gmail.com","name":"Abdula Morgunov","userId":3}""",
      """{"emailAddress":"patrik90melrose@mail.ru","name":"Patrick Melrose","userId":4}""",
      """{"emailAddress":"john_smith666@yahoo.com","name":"John Smith","userId":5}""",
      """{"emailAddress":"g1r1e1g_g1r1e1g1o1r1o1v@gmail.com","name":"Greg Gregorov","userId":6}""",
      """{"emailAddress":"masha_mashina@gmail.com","name":"Mariia Mashinova","userId":7}""",
      """{"emailAddress":"erick_gross@.com","name":"Erick Gross","userId":8}""",
      """{"emailAddress":"joshua_jackson123@gmail.com","name":"Joshua Jackson","userId":9}""",
      """{"emailAddress":"markkram@gmail.com","name":"Mark Kram","userId":10}"""
    )

    val meetings: List[String] = List(
      """{"endDate":1651360263344,"meetingId":1,"name":"Gathering","ownerId":1,"place":"Park","startDate":1651352263344,"frequency":46625544}""", //46625544
      """{"endDate":1651352263344,"meetingId":2,"name":"Birthday celebration","ownerId":9,"place":"Cinema","startDate":1651358263344}""",
      """{"endDate":1651381333444,"meetingId":3,"name":"Football Championship","ownerId":6,"startDate":1651362311834,"description":"championship for middle school students"}""",
      """{"endDate":1651388888888,"meetingId":4,"name":"Wizards meeting","ownerId":2,"startDate":1651367221238}"""
    )

    val invitations: List[String] = List(
      """{"invitationId":1,"inviteeId":2,"invitorId":1,"meetingId":1}""",
      """{"invitationId":2,"inviteeId":3,"invitorId":1,"meetingId":1}""",
      """{"invitationId":3,"inviteeId":9,"invitorId":2,"meetingId":4}""",
      """{"invitationId":4,"inviteeId":10,"invitorId":6,"meetingId":3}""",
      """{"invitationId":5,"inviteeId":3,"invitorId":1,"meetingId":3}""",
      """{"invitationId":6,"inviteeId":3,"invitorId":2,"meetingId":4}""",
      """{"invitationId":7,"inviteeId":7,"invitorId":1,"meetingId":1}""",
      """{"invitationId":8,"inviteeId":4,"invitorId":6,"meetingId":3}""",
      """{"invitationId":9,"inviteeId":1,"invitorId":2,"meetingId":4}"""
    )

    "be able to add users" in {

      val httpEntities = users.map(user => HttpEntity(ContentTypes.`application/json`, user))

      httpEntities.map(httpEntity =>
        Post("/user").withEntity(httpEntity) ~> server.routes ~> check {
          status shouldEqual StatusCodes.OK

          responseAs[String] shouldEqual "user created"
        }
      )
    }

    "be able to extract users" in {
      val usersId = List(7, 2, 4, 10)

      usersId.map(id =>
        Get(s"/user/$id") ~> server.routes ~> check {
          status shouldEqual StatusCodes.OK

          responseAs[String].parseJson shouldEqual users(id - 1).parseJson
        }
      )
    }


    "be able to delete users" in {
      val id = 5
      Delete(s"/user/$id") ~> server.routes ~> check {
        status shouldEqual StatusCodes.OK

        responseAs[String] shouldEqual "user deleted"
      }

      Get(s"/user/$id") ~> server.routes ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "be able to update users" in {
      val id = 7
      val userJson =
        s"""{"emailAddress":"tred_b12@gmail.com","name":"Tred Tredson","userId":$id}"""

      val httpEntity = HttpEntity(ContentTypes.`application/json`, userJson)
      Put(s"/user").withEntity(httpEntity) ~> server.routes ~> check {
        status shouldEqual StatusCodes.OK

        responseAs[String] shouldEqual "user updated"
      }

      Get(s"/user/$id") ~> server.routes ~> check {
        responseAs[String].parseJson shouldEqual userJson.parseJson
      }
    }


    "be able to create meetings" in {
      val httpEntities = meetings.map(meeting => HttpEntity(ContentTypes.`application/json`, meeting))

      httpEntities.map(httpEntity =>
        Post("/meeting").withEntity(httpEntity) ~> server.routes ~> check {
          status shouldEqual StatusCodes.OK

          responseAs[String] shouldEqual "meeting created"
        }
      )
    }


    "be able to extract meetings" in {
      val meetingsId = List(1, 2, 3, 4)

      meetingsId.map(id =>
        Get(s"/meeting/$id") ~> server.routes ~> check {
          status shouldEqual StatusCodes.OK

          responseAs[String].parseJson shouldEqual
            addJsonAttribute(meetings(id - 1), "status", "Confirmed").parseJson
        }
      )
    }

    "be able to delete meetings" in {
      val id = 2

      Delete(s"/meeting/$id") ~> server.routes ~> check {
        status shouldEqual StatusCodes.OK

        responseAs[String] shouldEqual "meeting deleted"
      }

      Get(s"/meeting/$id") ~> server.routes ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }


    "be able to update meetings" in {
      val id = 4
      val meetingJson =
        """{"endDate":1651388888888,"meetingId":4,"name":"Witches meeting","ownerId":2,"startDate":1651352263344}"""

      val httpEntity = HttpEntity(ContentTypes.`application/json`, meetingJson)
      Put(s"/meeting").withEntity(httpEntity) ~> server.routes ~> check {
        status shouldEqual StatusCodes.OK

        responseAs[String] shouldEqual "meeting updated"
      }

      Get(s"/meeting/$id") ~> server.routes ~> check {
        status shouldEqual StatusCodes.OK

        responseAs[String].parseJson shouldEqual
          addJsonAttribute(meetingJson, "status", "Confirmed").parseJson
      }
    }

    "be able to send invitations" in {
      val httpEntities = invitations.map(invitation => HttpEntity(ContentTypes.`application/json`, invitation))

      httpEntities.map(httpEntity =>
        Post("/invitation").withEntity(httpEntity) ~> server.routes ~> check {
          status shouldEqual StatusCodes.OK

          responseAs[String] shouldEqual "invitation created"
        }
      )

      val meetingsId = List(1, 3)

      meetingsId.map(id =>
        Get(s"/meeting/$id") ~> server.routes ~> check {
          status shouldEqual StatusCodes.OK

          responseAs[String].parseJson shouldEqual
            addJsonAttribute(meetings(id - 1), "status", "In doubt").parseJson
        }
      )
    }


    "be able to extract invitations" in {
      val invitationsId = List(1, 2, 3, 4)

      invitationsId.map(id =>
        Get(s"/invitation/$id") ~> server.routes ~> check {
          status shouldEqual StatusCodes.OK

          responseAs[String].parseJson shouldEqual invitations(id - 1).parseJson
        }
      )
    }

    "be able to delete invitations" in {
      val id = 3

      Delete(s"/invitation/$id") ~> server.routes ~> check {
        status shouldEqual StatusCodes.OK

        responseAs[String] shouldEqual "invitation deleted"
      }

      Get(s"/invitation/$id") ~> server.routes ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "be able to accept invitation" in {
      val id = 1
      val receivedStatus = "Busy"

      Put(s"/accept-invitation/?invitation-id=$id&received-status=$receivedStatus") ~> server.routes ~> check {
        status shouldEqual StatusCodes.OK

        responseAs[String] shouldEqual "invitation accepted"
      }

      Get(s"/invitation/$id") ~> server.routes ~> check {
        status shouldEqual StatusCodes.OK

        responseAs[String].parseJson shouldEqual
          s"""{"invitationId":1,"inviteeId":2,"invitorId":1,"meetingId":1,"accepted":true,"receivedStatus":"$receivedStatus"}""".parseJson

      }
    }

    "be able to decline invitation" in {
      val id = 2

      Put(s"/decline-invitation/$id") ~> server.routes ~> check {
        status shouldEqual StatusCodes.OK

        responseAs[String] shouldEqual "invitation declined"
      }

      Get(s"/invitation/$id") ~> server.routes ~> check {
        status shouldEqual StatusCodes.OK

        responseAs[String].parseJson shouldEqual
          """{"invitationId":2,"inviteeId":3,"invitorId":1,"meetingId":1,"accepted":false}""".parseJson
      }
    }

    "be able to find all the meetings in a time interval" in {
      val invitationsAcceptData: List[(Long, String)] = List((5, "Out-of-place"), (6, "Free"))

      invitationsAcceptData.map(data =>
        Put(s"/accept-invitation/?invitation-id=${data._1}&received-status=${data._2}") ~> server.routes ~> check {
          status shouldEqual StatusCodes.OK

          responseAs[String] shouldEqual "invitation accepted"
        }
      )

      val getRequest =
        Get(s"/time-interval-meetings/?user-id=3&start-time=1651352263344&end-time=1651398263345")

      getRequest ~> server.routes ~> check {
        status shouldEqual StatusCodes.OK

        responseAs[String].parseJson shouldEqual "[3,4]".parseJson
      }

      Get(s"/time-interval-meetings/?user-id=3&start-time=0&end-time=10000000") ~> server.routes ~> check {
        status shouldEqual StatusCodes.OK

        responseAs[String] shouldEqual "no meetings"
      }

      val meetingJson =
        """{"endDate":1651388888888,"meetingId":4,"name":"Witches meeting","ownerId":2,"startDate":1651367221238,"place":"forest"}"""

      val httpEntity = HttpEntity(ContentTypes.`application/json`, meetingJson)
      Put(s"/meeting").withEntity(httpEntity) ~> server.routes ~> check {
        status shouldEqual StatusCodes.OK

        responseAs[String] shouldEqual "meeting updated"
      }

      getRequest ~> server.routes ~> check {
        status shouldEqual StatusCodes.OK

        responseAs[String].parseJson shouldEqual "[3]".parseJson
      }
    }

    "be able to find appropriate meeting time for multiple users" in {
      val invitationsId: List[Long] = List(7, 8, 9)
      val meetingDurations = List[Long](
        9000000L, 12188380L, 50625544L
      )

      invitationsId.map(id =>
        Put(s"/accept-invitation/?invitation-id=$id&received-status=Busy") ~> server.routes ~> check {
          status shouldEqual StatusCodes.OK

          responseAs[String] shouldEqual "invitation accepted"
        }
      )

      Get(s"/closest-suitable-meeting/?duration=${meetingDurations.head}&user-id=7&user-id=4&user-id=1") ~>
        server.routes ~> check {
        val startTime: Long = 1651388888888L
        val endTime: Long = startTime + meetingDurations.head
        status shouldEqual StatusCodes.OK

        responseAs[String].parseJson shouldEqual
          s"""{"start":$startTime,"end":$endTime}""".parseJson
      }

      Get(s"/closest-suitable-meeting/?duration=38625543&user-id=7&user-id=1") ~>
        server.routes ~> check {
        val startTime: Long = 1651406888888L
        val endTime: Long = startTime + 38625543L
        status shouldEqual StatusCodes.OK

        responseAs[String].parseJson shouldEqual
          s"""{"start":$startTime,"end":$endTime}""".parseJson
      }

      Get(s"/closest-suitable-meeting/?duration=${meetingDurations.head}&user-id=7&user-id=4") ~>
        server.routes ~> check {
        val startTime: Long = 1651381333444L
        val endTime: Long = startTime + meetingDurations.head
        status shouldEqual StatusCodes.OK

        responseAs[String].parseJson shouldEqual
          s"""{"start":$startTime,"end":$endTime}""".parseJson
      }

      Put("/accept-invitation/?invitation-id=6&received-status=Busy") ~> server.routes ~> check {
        status shouldEqual StatusCodes.OK

        responseAs[String] shouldEqual "invitation accepted"
      }

      Get(s"/closest-suitable-meeting/?duration=${meetingDurations(1)}&user-id=3&user-id=4") ~>
        server.routes ~> check {
        val startTime: Long = 1651388888888L
        val endTime: Long = startTime + meetingDurations(1)
        status shouldEqual StatusCodes.OK

        responseAs[String].parseJson shouldEqual
          s"""{"start":$startTime,"end":$endTime}""".parseJson
      }

      //meeting is bigger time between repeating meetings
      Get(s"/closest-suitable-meeting/?duration=${meetingDurations(2)}&user-id=7&user-id=4&user-id=1") ~>
        server.routes ~> check {
        status shouldEqual StatusCodes.BadRequest
      }
    }

    "be able to cancel acceptance if essential information of a meeting is updated" in {
      val meetingVariations = List (
        """{"endDate":1651388888889,"meetingId":4,"name":"Witches meeting","ownerId":2,"startDate":1651367221238,"place":"forest"}""",
        """{"endDate":1651388888889,"meetingId":4,"name":"Witches meeting","ownerId":2,"startDate":1651367221238,"place":"playground"}"""
      )

      val httpEntities = meetingVariations.map(json => HttpEntity(ContentTypes.`application/json`, json))

      httpEntities.map( entity => {
        Put(s"/meeting").withEntity(entity) ~> server.routes ~> check {
          status shouldEqual StatusCodes.OK

          responseAs[String] shouldEqual "meeting updated"
        }

        Get(s"/invitation/6") ~> server.routes ~> check {
          status shouldEqual StatusCodes.OK

          responseAs[String].parseJson shouldEqual
            """{"invitationId":6,"inviteeId":3,"invitorId":2,"meetingId":4}""".parseJson
        }

        Put(s"/accept-invitation/?invitation-id=6&received-status=Busy") ~> server.routes ~> check {
          status shouldEqual StatusCodes.OK

          responseAs[String] shouldEqual "invitation accepted"
        }
      })
    }

    "be able to reevaluate meeting status if a user is deleted" in {
      Delete("/user/1") ~> server.routes ~> check {
        status shouldEqual StatusCodes.OK

        responseAs[String] shouldEqual "user deleted"
      }

      Get("/meeting/1") ~> server.routes ~> check {
        status shouldEqual StatusCodes.NotFound
      }

      Get("/meeting/3") ~> server.routes ~> check {
        status shouldEqual StatusCodes.OK

        responseAs[String].parseJson shouldEqual
          """{"endDate":1651381333444,"meetingId":3,"name":"Football Championship","ownerId":6,"startDate":1651362311834,"description":"championship for middle school students","status":"In doubt"}""".parseJson

      }

      Get("/meeting/4") ~> server.routes ~> check {
        status shouldEqual StatusCodes.OK

        responseAs[String].parseJson shouldEqual
          """{"endDate":1651388888889,"meetingId":4,"name":"Witches meeting","ownerId":2,"startDate":1651367221238,"place":"playground","status":"Confirmed"}""".parseJson
      }
    }

    "be able to create default user repository" in {
      new DatabaseUserRepository(configPrefix)
    }
  }
}