CREATE DATABASE IF NOT EXISTS `database`;
drop database if exists `test`;
CREATE DATABASE `test`;
drop user 'root'@'%';
flush privileges;
CREATE USER 'root'@'%' IDENTIFIED BY 'pass';
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;

use `database`;

create table if not exists user (
    user_id bigint primary key,
    name varchar(30) not null,
    email_address varchar(50) not null
);

create table if not exists meeting (
    meeting_id bigint primary key,
    name varchar(30) not null,
    owner_id bigint not null,
    start_date bigint not null,
    end_date bigint not null,
    place varchar(30),
    description varchar(200),
    frequency bigint,
    status varchar(20),
    foreign key (owner_id) references user(user_id)
        on delete cascade
);


create table if not exists invitation (
    invitation_id bigint primary key,
    invitor_id bigint not null,
    invitee_id bigint not null,
    meeting_id bigint not null,
    accepted bool,
    received_status varchar(30),
    foreign key (invitor_id) references user(user_id)
        on delete cascade,
    foreign key (invitee_id) references user(user_id)
        on delete cascade,
    foreign key (meeting_id) references meeting(meeting_id)
        on delete cascade
);

use `test`;

create table user (
    user_id bigint primary key,
    name varchar(30) not null,
    email_address varchar(50) not null
);

create table meeting (
    meeting_id bigint primary key,
    name varchar(30) not null,
    owner_id bigint not null,
    start_date bigint not null,
    end_date bigint not null,
    place varchar(30),
    description varchar(200),
    frequency bigint,
    status varchar(20),
    foreign key (owner_id) references user(user_id)
        on delete cascade
);


create table invitation (
    invitation_id bigint primary key,
    invitor_id bigint not null,
    invitee_id bigint not null,
    meeting_id bigint not null,
    accepted bool,
    received_status varchar(30),
    foreign key (invitor_id) references user(user_id)
        on delete cascade,
    foreign key (invitee_id) references user(user_id)
        on delete cascade,
    foreign key (meeting_id) references meeting(meeting_id)
        on delete cascade
);