FROM mysql:8.0
ENV MYSQL_DATABASE=database
ENV MYSQL_ROOT_PASSWORD=pass
EXPOSE 3306
ADD ./init.sql /docker-entrypoint-initdb.d/
